package com.rahasak.etcd3l.config

import com.typesafe.config.ConfigFactory
import org.etcd4s.Etcd4sClientConfig

import scala.util.Try

trait EtcdConf {
  val config = ConfigFactory.load("etcd.conf")

  lazy val etcdHost = Try(config.getString("etcd.host")).getOrElse("dev.localhost")
  lazy val etcdPort = Try(config.getInt("etcd.port")).getOrElse(2379)

  lazy val etcdConf = Etcd4sClientConfig(
    address = etcdHost,
    port = etcdPort
  )
}
