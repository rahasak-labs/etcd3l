package com.rahasak.etcd3l.etcd

import com.google.protobuf.ByteString
import com.rahasak.etcd3l.config.EtcdConf
import io.grpc.stub.StreamObserver
import org.etcd4s.Etcd4sClient
import org.etcd4s.pb.etcdserverpb._

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object EtcdStore extends EtcdConf {
  val client = Etcd4sClient.newClient(etcdConf)

  /**
   * put key/value with etcd3 grpc api
   */
  def put(key: String, value: String): PutResponse = {
    val f = client.kvApi.put(PutRequest()
      .withKey(ByteString.copyFromUtf8(key))
      .withValue(ByteString.copyFromUtf8(value)))

    Await.result(f, 10.seconds)
  }

  /**
   * put key/value/ttl with etcd3 grpc api
   * lease created with lease api, then lease id assigned to key/value
   */
  def putWithTTL(key: String, value: String, ttl: Long): Long = {
    val f = for {
      id <- client.leaseApi.leaseGrant(LeaseGrantRequest(ttl)).map(_.iD)
      _ <- client.kvApi.put(PutRequest()
        .withKey(ByteString.copyFromUtf8(key))
        .withValue(ByteString.copyFromUtf8(value))
        .withLease(id)
      )
    } yield id

    Await.result(f, 10.seconds)
  }

  /**
   * refresh lease with grpc streaming api
   */
  def refreshLeaseTTL(leaseId: Long): Unit = {
    val observer = new StreamObserver[LeaseKeepAliveResponse] {
      override def onNext(value: LeaseKeepAliveResponse) = {}

      override def onError(t: Throwable) = {}

      override def onCompleted() = {}
    }

    val request = client.leaseApi.leaseKeepAlive(observer)
    request.onNext(LeaseKeepAliveRequest.of(leaseId))
  }

  /**
   * get value with given key via grpc api
   */
  def get(key: String): Option[String] = {
    val f = client.kvApi.range(RangeRequest()
      .withKey(ByteString.copyFromUtf8(key)))
      .map(_.kvs.headOption.map(_.value.toString("UTF-8")))

    Await.result(f, 10.seconds)
  }

  /**
   * get keys with given prefix(range search) via grpc api
   */
  def getRange(key: String): Seq[String] = {
    val f = client.getRange(key).map(r => r.kvs.map(_.value.toString("UTF-8")))

    Await.result(f, 10.seconds)
  }

}

object Main extends App {

  // create key/value without ttl
  val resp = EtcdStore.put("lokka/lokka1", "rahasak")
  println(resp.header.get.toString)
  // output
  // ResponseHeader(-3605105004744373198,-8170086329776576179,2,2)

  // get key
  println(EtcdStore.get("lokka/lokka1"))
  // output
  // Some(rahasak)

  // create key/value with ttl 10 seconds
  // once lease expires this key/value should be disappear
  val leaseId = EtcdStore.putWithTTL("lokka/lokka2", "librum", 10)
  println(leaseId)
  // output
  // 7587862470829408734

  // get key
  println(EtcdStore.get("lokka/lokka2"))
  // output
  // Some(librum)

  // get range
  // tow keys should be there
  println(EtcdStore.getRange("lokka"))
  // output
  // Vector(rahasak, librum)

  // refresh lease
  EtcdStore.refreshLeaseTTL(leaseId)

  // sleep 25 seconds time
  Thread.sleep(25000)

  // get range
  // lokka/lokka2 should be disappear now
  println(EtcdStore.getRange("lokka"))
  // output
  // Vector(rahasak)

}
